package com.dungeons.dungeon1.dungeonscode;

import androidx.appcompat.app.AppCompatActivity;

import android.animation.Animator;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.dungeons.dungeon1.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

public class Dungeon extends AppCompatActivity {

    private LinearLayout mainLayout;
    private RelativeLayout relative;
    private TextView message_box;
    private LinearLayout goUp, goDown, goLeft, goRight;
    private LinearLayout[][] cells;
    private Tile[][] tiles;
    int width;
    int height;

    boolean fixedSize = false;

    int rows = 12;
    int cols = 12;

    int startingI, startingJ;
    int floorRows = 0, floorCols = 0;

    LinearLayout player;
    ImageView playerImage;

    LinearLayout currentTile;
    Tile currentTileee;

    SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dungeon);

        mainLayout = findViewById(R.id.mainLayout);
        relative = findViewById(R.id.relative);
        message_box = findViewById(R.id.message_box);
        goDown = findViewById(R.id.goDown);
        goUp = findViewById(R.id.goUp);
        goLeft = findViewById(R.id.goLeft);
        goRight = findViewById(R.id.goRight);

        preferences = Dungeon.this.getPreferences(MODE_PRIVATE);

        implementCrossListeners();

        mainLayout.setOrientation(LinearLayout.VERTICAL);
        mainLayout.setGravity(Gravity.CENTER);

        TextView tv = new TextView(Dungeon.this);
        tv.setText("hoplaaaaaaaa");
        relative.setGravity(RelativeLayout.CENTER_HORIZONTAL);
        relative.addView(tv);

        if (!fixedSize) {
            Random r = new Random();
            rows = 8 + r.nextInt(10);
            cols = rows;
        }

        cells = new LinearLayout[rows][cols];
        tiles = new Tile[rows][cols];

        for (int i = 0; i < cells.length; i++) {

            LinearLayout fila = new LinearLayout(Dungeon.this);
            fila.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            fila.setOrientation(LinearLayout.HORIZONTAL);

            for (int j = 0; j < cells[i].length; j++) {
                cells[i][j] = new LinearLayout(Dungeon.this);
                cells[i][j].setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                cells[i][j].setOrientation(LinearLayout.HORIZONTAL);
                fila.addView(cells[i][j]);
            }

            mainLayout.addView(fila);

        }

        final View content = findViewById(android.R.id.content);
        content.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                content.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                width = content.getMeasuredWidth();
                height = content.getMeasuredHeight();

                for (int i = 0; i < cells.length; i++) {
                    for (int j = 0; j < cells[i].length; j++) {
                        ViewGroup.LayoutParams layoutParams = cells[i][j].getLayoutParams();
                        layoutParams.width = width/cells[0].length;
                        layoutParams.height = width/cells[0].length;
                        cells[i][j].setLayoutParams(layoutParams);
                        tiles[i][j] = new Tile(false);
                    }
                }

                Random r = new Random();
                LinearLayout startingPoint = cells[1 + r.nextInt(rows-2)][1 + r.nextInt(cols - 2)];

                String whichWarp = getIntent().getStringExtra("whichWarp");

                startingI = 1 + r.nextInt(rows/2 - 1);
                startingJ = 1 + r.nextInt(cols/2 - 1);
                tiles[startingI][startingJ].setFloor(true);
                tiles[startingI][startingJ].setDirection("floor");
                spawnPlayer(cells[startingI][startingJ], "down");


                while (floorRows < (rows/2) - 1 || floorCols < (cols/2) - 1) {
                    floorRows = r.nextInt(rows - startingI);
                    floorCols = r.nextInt(cols - startingJ);
                }

                setFloorAndWalls();
                giveGenericWallsItsDirection();
                buildWalls();
                buildCorners();
                createRoads();
                createRoadWalls();
                implementListeners();
                changePlayerCordsAfterWarp(whichWarp, r);
                rebuildAfterWarp();
                generateItems();
                generateTraps();

            }
        });

    }

    private void generateItems() {

        Random r = new Random();
        int x = 0;
        int y = 0;
        int spawnItem = 0;

        boolean key1 = preferences.getBoolean("Llave 1", false);
        boolean key2 = preferences.getBoolean("Llave 2", false);
        boolean key3 = preferences.getBoolean("Llave 3", false);
        spawnItem = r.nextInt(8);

        Toast.makeText(Dungeon.this, "Spawn:" + spawnItem, Toast.LENGTH_SHORT).show();

        if (spawnItem == 1) {

            if (!key1) {

                while (!tiles[x][y].isFloor() || cells[x][y] == currentTile || !tiles[x][y].getDirection().equals("floor")) {
                    x = r.nextInt(floorRows) + 1;
                    y = r.nextInt(floorCols) + 1;
                }

                LinearLayout item = new LinearLayout(Dungeon.this);
                item.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                item.setBackground(getDrawable(R.drawable.key));

                cells[x][y].addView(item);
                tiles[x][y].setDirection("key1");

            } else if (!key2) {

                while (!tiles[x][y].isFloor() || cells[x][y] == currentTile || !tiles[x][y].getDirection().equals("floor")) {
                    x = r.nextInt(floorRows) + 1;
                    y = r.nextInt(floorCols) + 1;
                }

                LinearLayout item = new LinearLayout(Dungeon.this);
                item.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                item.setBackground(getDrawable(R.drawable.key));

                cells[x][y].addView(item);
                tiles[x][y].setDirection("key2");

            } else if (!key3) {

                while (!tiles[x][y].isFloor() || cells[x][y] == currentTile || !tiles[x][y].getDirection().equals("floor")) {
                    x = r.nextInt(floorRows) + 1;
                    y = r.nextInt(floorCols) + 1;
                }

                LinearLayout item = new LinearLayout(Dungeon.this);
                item.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                item.setBackground(getDrawable(R.drawable.key));

                cells[x][y].addView(item);
                tiles[x][y].setDirection("key3");

            }

        }

    }

    private void generateTraps() {

        Random r = new Random();
        int x = 0;
        int y = 0;
        int spawnTrap = 0;

        spawnTrap = r.nextInt(2);

        Toast.makeText(Dungeon.this, "Spawn trap:" + spawnTrap, Toast.LENGTH_SHORT).show();

        if (spawnTrap == 1) {

            while (!tiles[x][y].isFloor() || cells[x][y] == currentTile || !tiles[x][y].getDirection().equals("floor")) {
                x = r.nextInt(floorRows) + 1;
                y = r.nextInt(floorCols) + 1;
            }

            LinearLayout item = new LinearLayout(Dungeon.this);
            item.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
            item.setBackground(getDrawable(R.drawable.key));

            cells[x][y].addView(item);
            tiles[x][y].setDirection("trap");

        }

    }

    private void rebuildAfterWarp() {
        for (int i = 0; i < cells.length; i++) {
            for (int j = 0; j < cells[i].length; j++) {
                if (tiles[i][j].getDirection().equals("southnotwarpable")) {

                    if (tiles[i][j - 1].getDirection().equals("west")) {
                        cells[i][j].setBackground(getResources().getDrawable(R.drawable.floor));
                        tiles[i][j].setDirection("south");
                        tiles[i][j].setFloor(false);

                        LinearLayout temporalFloor = new LinearLayout(Dungeon.this);
                        temporalFloor.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                        temporalFloor.setBackground(getDrawable(R.drawable.southwall));
                        cells[i][j].addView(temporalFloor);

                        Animation animation = new TranslateAnimation(cells[i][j].getX() + width/cells[0].length, cells[i][j].getX(), cells[i][j].getY(), cells[i][j].getY());
                        animation.setDuration(750);
                        temporalFloor.startAnimation(animation);
                        cells[i][j - 1].setBackground(getResources().getDrawable(R.drawable.bottomleftcorner));
                        tiles[i][j - 1].setDirection("bottomleft");
                        tiles[i][j - 1].setFloor(false);
                        cells[i][j + 1].setBackground(getResources().getDrawable(R.drawable.bottomrightcorner));
                        tiles[i][j + 1].setDirection("bottomright");
                        tiles[i][j + 1].setFloor(false);
                    } else {
                        cells[i][j].setBackground(getResources().getDrawable(R.drawable.floor));
                        tiles[i][j].setDirection("south");
                        tiles[i][j].setFloor(false);

                        LinearLayout temporalFloor = new LinearLayout(Dungeon.this);
                        temporalFloor.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                        temporalFloor.setBackground(getDrawable(R.drawable.southwall));
                        cells[i][j].addView(temporalFloor);

                        Animation animation = new TranslateAnimation(cells[i][j].getX() + width/cells[0].length, cells[i][j].getX(), cells[i][j].getY(), cells[i][j].getY());
                        animation.setDuration(750);
                        temporalFloor.startAnimation(animation);
                        cells[i][j - 1].setBackground(getResources().getDrawable(R.drawable.southwall));
                        tiles[i][j - 1].setDirection("south");
                        tiles[i][j - 1].setFloor(false);
                        cells[i][j + 1].setBackground(getResources().getDrawable(R.drawable.southwall));
                        tiles[i][j + 1].setDirection("south");
                        tiles[i][j + 1].setFloor(false);
                    }

                    message_box.setText(getString(R.string.wall_closed));

                } else if (tiles[i][j].getDirection().equals("northnotwarpable")) {
                    if (tiles[i][j - 1].getDirection().equals("west")) {
                        cells[i][j].setBackground(getResources().getDrawable(R.drawable.floor));
                        tiles[i][j].setDirection("north");
                        tiles[i][j].setFloor(false);

                        LinearLayout temporalFloor = new LinearLayout(Dungeon.this);
                        temporalFloor.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                        temporalFloor.setBackground(getDrawable(R.drawable.northwall));
                        cells[i][j].addView(temporalFloor);

                        Animation animation = new TranslateAnimation(cells[i][j].getX() + width/cells[0].length, cells[i][j].getX(), cells[i][j].getY(), cells[i][j].getY());
                        animation.setDuration(750);
                        temporalFloor.startAnimation(animation);

                        cells[i][j - 1].setBackground(getResources().getDrawable(R.drawable.topleftcorner));
                        tiles[i][j - 1].setDirection("topleft");
                        tiles[i][j - 1].setFloor(false);
                        cells[i][j + 1].setBackground(getResources().getDrawable(R.drawable.toprightcorner));
                        tiles[i][j + 1].setDirection("topright");
                        tiles[i][j + 1].setFloor(false);
                    } else {
                        cells[i][j].setBackground(getResources().getDrawable(R.drawable.floor));
                        tiles[i][j].setDirection("north");
                        tiles[i][j].setFloor(false);

                        LinearLayout temporalFloor = new LinearLayout(Dungeon.this);
                        temporalFloor.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                        temporalFloor.setBackground(getDrawable(R.drawable.northwall));
                        cells[i][j].addView(temporalFloor);

                        Animation animation = new TranslateAnimation(cells[i][j].getX() + width/cells[0].length, cells[i][j].getX(), cells[i][j].getY(), cells[i][j].getY());
                        animation.setDuration(750);
                        temporalFloor.startAnimation(animation);

                        cells[i][j - 1].setBackground(getResources().getDrawable(R.drawable.northwall));
                        tiles[i][j - 1].setDirection("north");
                        tiles[i][j - 1].setFloor(false);
                        cells[i][j + 1].setBackground(getResources().getDrawable(R.drawable.northwall));
                        tiles[i][j + 1].setDirection("north");
                        tiles[i][j + 1].setFloor(false);
                    }

                    message_box.setText(getString(R.string.wall_closed));

                } else if (tiles[i][j].getDirection().equals("westnotwarpable")) {
                    if (tiles[i - 1][j].getDirection().equals("north")) {
                        cells[i][j].setBackground(getResources().getDrawable(R.drawable.floor));
                        tiles[i][j].setDirection("west");
                        tiles[i][j].setFloor(false);

                        LinearLayout temporalFloor = new LinearLayout(Dungeon.this);
                        temporalFloor.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                        temporalFloor.setBackground(getDrawable(R.drawable.westwall));
                        cells[i][j].addView(temporalFloor);

                        Animation animation = new TranslateAnimation(cells[i][j].getX(), cells[i][j].getX(), cells[i][j].getY() - width/cells[0].length, cells[i][j].getY());
                        animation.setDuration(750);
                        temporalFloor.startAnimation(animation);

                        cells[i - 1][j].setBackground(getResources().getDrawable(R.drawable.topleftcorner));
                        tiles[i - 1][j].setDirection("topleft");
                        tiles[i - 1][j].setFloor(false);
                        cells[i + 1][j].setBackground(getResources().getDrawable(R.drawable.bottomleftcorner));
                        tiles[i + 1][j].setDirection("bottomleft");
                        tiles[i + 1][j].setFloor(false);
                    } else {
                        cells[i][j].setBackground(getResources().getDrawable(R.drawable.floor));
                        tiles[i][j].setDirection("west");
                        tiles[i][j].setFloor(false);

                        LinearLayout temporalFloor = new LinearLayout(Dungeon.this);
                        temporalFloor.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                        temporalFloor.setBackground(getDrawable(R.drawable.westwall));
                        cells[i][j].addView(temporalFloor);

                        Animation animation = new TranslateAnimation(cells[i][j].getX(), cells[i][j].getX(), cells[i][j].getY() - width/cells[0].length, cells[i][j].getY());
                        animation.setDuration(750);
                        temporalFloor.startAnimation(animation);

                        cells[i - 1][j].setBackground(getResources().getDrawable(R.drawable.westwall));
                        tiles[i - 1][j].setDirection("west");
                        tiles[i - 1][j].setFloor(false);
                        cells[i + 1][j].setBackground(getResources().getDrawable(R.drawable.westwall));
                        tiles[i + 1][j].setDirection("west");
                        tiles[i + 1][j].setFloor(false);
                    }

                    message_box.setText(getString(R.string.wall_closed));

                } else if (tiles[i][j].getDirection().equals("eastnotwarpable")) {
                    if (tiles[i - 1][j].getDirection().equals("north")) {
                        cells[i][j].setBackground(getResources().getDrawable(R.drawable.floor));
                        tiles[i][j].setDirection("east");
                        tiles[i][j].setFloor(false);

                        LinearLayout temporalFloor = new LinearLayout(Dungeon.this);
                        temporalFloor.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                        temporalFloor.setBackground(getDrawable(R.drawable.eastwall));
                        cells[i][j].addView(temporalFloor);

                        Animation animation = new TranslateAnimation(cells[i][j].getX(), cells[i][j].getX(), cells[i][j].getY() - width/cells[0].length, cells[i][j].getY());
                        animation.setDuration(750);
                        temporalFloor.startAnimation(animation);

                        cells[i - 1][j].setBackground(getResources().getDrawable(R.drawable.toprightcorner));
                        tiles[i - 1][j].setDirection("topright");
                        tiles[i - 1][j].setFloor(false);
                        cells[i + 1][j].setBackground(getResources().getDrawable(R.drawable.bottomrightcorner));
                        tiles[i + 1][j].setDirection("bottomright");
                        tiles[i + 1][j].setFloor(false);
                    } else {
                        cells[i][j].setBackground(getResources().getDrawable(R.drawable.floor));
                        tiles[i][j].setDirection("east");
                        tiles[i][j].setFloor(false);

                        LinearLayout temporalFloor = new LinearLayout(Dungeon.this);
                        temporalFloor.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                        temporalFloor.setBackground(getDrawable(R.drawable.eastwall));
                        cells[i][j].addView(temporalFloor);

                        Animation animation = new TranslateAnimation(cells[i][j].getX(), cells[i][j].getX(), cells[i][j].getY() - width/cells[0].length, cells[i][j].getY());
                        animation.setDuration(750);
                        temporalFloor.startAnimation(animation);

                        cells[i - 1][j].setBackground(getResources().getDrawable(R.drawable.eastwall));
                        tiles[i - 1][j].setDirection("east");
                        tiles[i - 1][j].setFloor(false);
                        cells[i + 1][j].setBackground(getResources().getDrawable(R.drawable.eastwall));
                        tiles[i + 1][j].setDirection("east");
                        tiles[i + 1][j].setFloor(false);
                    }

                    message_box.setText(getString(R.string.wall_closed));

                }

            }
        }
    }

    private void changePlayerCordsAfterWarp(String whichWarp, Random r) {
        try {
            switch (whichWarp) {

                case "northwarp":
                    for (int i = 0; i < cells.length; i++) {
                        for (int j = 0; j < cells[i].length; j++) {
                            if (tiles[i][j].getDirection().equals("southwarp")) {
                                cells[startingI][startingJ].removeAllViews();
                                tiles[i][j].setFloor(false);
                                tiles[i][j].setDirection("southnotwarpable");
                                spawnPlayer(cells[i - 1][j], "up");
                                break;
                            }
                        }
                    }
                    break;

                case "southwarp":
                    for (int i = 0; i < cells.length; i++) {
                        for (int j = 0; j < cells[i].length; j++) {
                            if (tiles[i][j].getDirection().equals("northwarp")) {
                                cells[startingI][startingJ].removeAllViews();
                                tiles[i][j].setFloor(false);
                                tiles[i][j].setDirection("northnotwarpable");
                                spawnPlayer(cells[i + 1][j], "down");
                                break;
                            }
                        }
                    }
                    break;

                case "westwarp":
                    for (int i = 0; i < cells.length; i++) {
                        for (int j = 0; j < cells[i].length; j++) {
                            if (tiles[i][j].getDirection().equals("eastwarp")) {
                                cells[startingI][startingJ].removeAllViews();
                                tiles[i][j].setFloor(false);
                                tiles[i][j].setDirection("eastnotwarpable");
                                spawnPlayer(cells[i][j - 1], "left");
                                break;
                            }
                        }
                    }
                    break;

                case "eastwarp":
                    for (int i = 0; i < cells.length; i++) {
                        for (int j = 0; j < cells[i].length; j++) {
                            if (tiles[i][j].getDirection().equals("westwarp")) {
                                cells[startingI][startingJ].removeAllViews();
                                tiles[i][j].setFloor(false);
                                tiles[i][j].setDirection("westnotwarpable");
                                spawnPlayer(cells[i][j + 1], "right");
                                break;
                            }
                        }
                    }
                    break;
            }
        } catch (Exception e) {
        }
    }

    private void createRoadWalls() {

        for (int i = 0; i < cells.length; i++) {
            for (int j = 0; j < cells[i].length; j++) {

                try {

                    //Norte
                    if (j > 0 && j < tiles[i].length - 1) {
                        if (tiles[i][j - 1].getDirection().equals("wall") && (tiles[i][j].getDirection().equals("road") || tiles[i][j].getDirection().contains("warp")) && tiles[i][j + 1].getDirection().equals("wall")) {
                            cells[i][j - 1].setBackground(getResources().getDrawable(R.drawable.westwall));
                            tiles[i][j - 1].setDirection("west");
                            cells[i][j + 1].setBackground(getResources().getDrawable(R.drawable.eastwall));
                            tiles[i][j + 1].setDirection("east");
                            if (tiles[i + 1][j - 1].getDirection().equals("north")) {
                                cells[i + 1][j - 1].setBackground(getResources().getDrawable(R.drawable.bottomrightinvertedcorner));
                                tiles[i + 1][j - 1].setDirection("bottomrightinverted");
                                cells[i + 1][j + 1].setBackground(getResources().getDrawable(R.drawable.bottomleftinvertedcorner));
                                tiles[i + 1][j + 1].setDirection("bottomleftinverted");
                            }
                        } else if (tiles[i][j - 1].getDirection().equals("north") && (tiles[i][j].getDirection().equals("road") || tiles[i][j].getDirection().contains("warp")) && tiles[i][j + 1].getDirection().equals("north")) {
                            //Cuando esta la sala pegada al norte
                            if (tiles[i][j - 1].getDirection().equals("north")) {
                                cells[i][j - 1].setBackground(getResources().getDrawable(R.drawable.bottomrightinvertedcorner));
                                tiles[i][j - 1].setDirection("bottomrightinverted");
                                cells[i][j + 1].setBackground(getResources().getDrawable(R.drawable.bottomleftinvertedcorner));
                                tiles[i][j + 1].setDirection("bottomleftinverted");
                            }
                        } else if(tiles[i][j - 1].getDirection().equals("south") && (tiles[i][j].getDirection().equals("road") || tiles[i][j].getDirection().contains("warp")) && tiles[i][j + 1].getDirection().equals("south")) {
                            if (tiles[i][j - 1].getDirection().equals("south")) {
                                cells[i][j - 1].setBackground(getResources().getDrawable(R.drawable.toprightinvertedcorner));
                                tiles[i][j - 1].setDirection("toprightinverted");
                                cells[i][j + 1].setBackground(getResources().getDrawable(R.drawable.topleftinvertedcorner));
                                tiles[i][j + 1].setDirection("topleftinverted");
                            }
                        }
                    }

                    if (i > 0 && i < tiles.length - 1) {

                        if (tiles[i - 1][j].getDirection().equals("wall") && (tiles[i][j].getDirection().equals("road") || tiles[i][j].getDirection().contains("warp")) && tiles[i + 1][j].getDirection().equals("wall")) {

                            cells[i - 1][j].setBackground(getResources().getDrawable(R.drawable.northwall));
                            tiles[i - 1][j].setDirection("north");
                            cells[i + 1][j].setBackground(getResources().getDrawable(R.drawable.southwall));
                            tiles[i + 1][j].setDirection("south");

                        } else if (tiles[i - 1][j].getDirection().equals("west") && (tiles[i][j].getDirection().equals("road") || tiles[i][j].getDirection().contains("warp")) && tiles[i + 1][j].getDirection().equals("west")) {
                            cells[i + 1][j].setBackground(getResources().getDrawable(R.drawable.toprightinvertedcorner));
                            tiles[i + 1][j].setDirection("toprightinverted");
                            cells[i - 1][j].setBackground(getResources().getDrawable(R.drawable.bottomrightinvertedcorner));
                            tiles[i - 1][j].setDirection("bottomrightinverted");
                        } else if (tiles[i - 1][j].getDirection().equals("east") && (tiles[i][j].getDirection().equals("road") || tiles[i][j].getDirection().contains("warp")) && tiles[i + 1][j].getDirection().equals("east")) {
                            cells[i + 1][j].setBackground(getResources().getDrawable(R.drawable.topleftinvertedcorner));
                            tiles[i + 1][j].setDirection("topleftinverted");
                            cells[i - 1][j].setBackground(getResources().getDrawable(R.drawable.bottomleftinvertedcorner));
                            tiles[i - 1][j].setDirection("bottomleftinverted");
                        }

                    }

                }catch (Exception e) {

                }

            }
        }

    }

    private void createRoads() {

        ArrayList<Tile> northTiles = new ArrayList<>();
        ArrayList<Tile> southTiles = new ArrayList<>();
        ArrayList<Tile> westTiles = new ArrayList<>();
        ArrayList<Tile> eastTiles = new ArrayList<>();

        int northY = 0, southY = 0;
        int westX = 0, eastX = 0;

        //Toast.makeText(Dungeon.this, "Lienzo de " + floorRows + "x" + floorCols, Toast.LENGTH_SHORT).show();
        Random r = new Random();

        for (int i = 0; i < cells.length; i++) {
            for (int j = 0; j < cells[i].length; j++) {

                try {

                    if (tiles[i][j].getDirection().equals("north")) {
                        northY = i;
                        northTiles.add(tiles[i][j]);
                    }else if (tiles[i][j].getDirection().equals("south")) {
                        southY = i;
                        southTiles.add(tiles[i][j]);
                    } else if (tiles[i][j].getDirection().equals("west")) {
                        westX = j;
                        westTiles.add(tiles[i][j]);
                    } else if (tiles[i][j].getDirection().equals("east")) {
                        eastX = j;
                        eastTiles.add(tiles[i][j]);
                        Log.i("east", "a:" + eastX + " a: " + eastTiles.size());
                    }
                } catch (Exception e) {
                }

            }
        }

        try {
            for (int i = northY; i >= 0; i--) {
                if (i == 0) {
                    tiles[i][(startingJ + northTiles.size()/2)].setDirection("northwarp");
                } else {
                    tiles[i][(startingJ + northTiles.size()/2)].setDirection("road");
                }
                tiles[i][(startingJ + northTiles.size()/2)].setFloor(true);
                cells[i][(startingJ + northTiles.size()/2)].setBackground(getDrawable(R.drawable.floor));
            }
        } catch (Exception e){
        }

        try {
            for (int i = southY; i < cells.length; i++) {
                if (i == cells.length - 1) {
                    tiles[i][(startingJ + southTiles.size()/2)].setDirection("southwarp");
                } else {
                    tiles[i][(startingJ + southTiles.size()/2)].setDirection("road");
                }
                tiles[i][(startingJ + southTiles.size()/2)].setFloor(true);
                cells[i][(startingJ + southTiles.size()/2)].setBackground(getDrawable(R.drawable.floor));
            }
        } catch (Exception e){
        }

        try {
            for (int i = westX; i < cells[westX].length; i--) {
                if (i == 0) {
                    tiles[(startingI + westTiles.size()/2)][i].setDirection("westwarp");
                } else {
                    tiles[(startingI + westTiles.size()/2)][i].setDirection("road");
                }
                tiles[(startingI + westTiles.size()/2)][i].setFloor(true);
                cells[(startingI + westTiles.size()/2)][i].setBackground(getDrawable(R.drawable.floor));
            }
        } catch (Exception e){
        }

        try {
            for (int i = eastX; i < cells[eastX].length; i++) {
                if (i == cells[eastX].length - 1) {
                    tiles[(startingI + eastTiles.size()/2)][i].setDirection("eastwarp");
                } else {
                    tiles[(startingI + eastTiles.size()/2)][i].setDirection("road");
                }
                tiles[(startingI + eastTiles.size()/2)][i].setFloor(true);
                cells[(startingI + eastTiles.size()/2)][i].setBackground(getDrawable(R.drawable.floor));
            }
        } catch (Exception e){
        }

    }

    private void implementCrossListeners() {

        goUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                message_box.setText("");
                Glide.with(Dungeon.this).load(R.drawable.backcharacter).into(playerImage);
                checkIfCanMove("up");
            }
        });

        goDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                message_box.setText("");
                Glide.with(Dungeon.this).load(R.drawable.frontcharacter).into(playerImage);
                checkIfCanMove("down");
            }
        });

        goRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                message_box.setText("");
                Glide.with(Dungeon.this).load(R.drawable.rightcharacter).into(playerImage);
                checkIfCanMove("right");
            }
        });

        goLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                message_box.setText("");
                Glide.with(Dungeon.this).load(R.drawable.leftcharacter).into(playerImage);
                checkIfCanMove("left");
            }
        });

    }

    private boolean checkIfCanMove (String direction) {
        int indexI = 0, indexJ = 0;
        for (int i = 0 ; i < cells.length; i++) {
            for (int j = 0; j < cells.length; j++) {
                if (cells[i][j] == currentTile) {
                    indexI = i;
                    indexJ = j;
                    break;
                }
            }
        }
        if (tiles[indexI][indexJ].getDirection().equals("northwarp") && direction.equals("up")) {
            currentTile.removeAllViews();
            Intent i = new Intent(Dungeon.this, Dungeon.class);
            i.putExtra("whichWarp", "northwarp");
            startActivity(i);
            finish();
        } else if (tiles[indexI][indexJ].getDirection().equals("southwarp") && direction.equals("down")) {
            currentTile.removeAllViews();
            Intent i = new Intent(Dungeon.this, Dungeon.class);
            i.putExtra("whichWarp", "southwarp");
            startActivity(i);
            finish();
        } else if (tiles[indexI][indexJ].getDirection().equals("westwarp") && direction.equals("left")) {
            currentTile.removeAllViews();
            Intent i = new Intent(Dungeon.this, Dungeon.class);
            i.putExtra("whichWarp", "westwarp");
            startActivity(i);
            finish();
        } else if (tiles[indexI][indexJ].getDirection().equals("eastwarp") && direction.equals("right")) {
            currentTile.removeAllViews();
            Intent i = new Intent(Dungeon.this, Dungeon.class);
            i.putExtra("whichWarp", "eastwarp");
            startActivity(i);
            finish();
        }
        //Toast.makeText(Dungeon.this, "Index: " + indexI + " j: " + indexJ, Toast.LENGTH_SHORT).show();
        try {
            switch (direction) {

                case "up":
                    if (tiles[indexI - 1][indexJ].isFloor()) {
                        currentTile.removeAllViews();
                        currentTile = cells[indexI - 1][indexJ];
                        currentTileee = tiles[indexI - 1][indexJ];
                        handleItemObtaining();
                        spawnPlayer(cells[indexI - 1][indexJ], direction);
                        spawnTrapIfNeeded();
                    }
                    break;

                case "down":
                    if (tiles[indexI + 1][indexJ].isFloor()) {
                        currentTile.removeAllViews();
                        currentTile = cells[indexI + 1][indexJ];
                        currentTileee = tiles[indexI + 1][indexJ];
                        handleItemObtaining();
                        spawnPlayer(cells[indexI + 1][indexJ], direction);
                        spawnTrapIfNeeded();
                    }
                    break;

                case "left":
                    if (tiles[indexI][indexJ - 1].isFloor()) {
                        currentTile.removeAllViews();
                        currentTile = cells[indexI][indexJ - 1];
                        currentTileee = tiles[indexI][indexJ - 1];
                        handleItemObtaining();
                        spawnPlayer(cells[indexI][indexJ - 1], direction);
                        spawnTrapIfNeeded();
                    }
                    break;

                case "right":
                    if (tiles[indexI][indexJ + 1].isFloor()) {
                        currentTile.removeAllViews();
                        currentTile = cells[indexI][indexJ + 1];
                        currentTileee = tiles[indexI][indexJ + 1];
                        handleItemObtaining();
                        spawnPlayer(cells[indexI][indexJ + 1], direction);
                        spawnTrapIfNeeded();
                    }
                    break;

            }
        } catch (Exception e){
        }
        return true;
    }

    private void spawnTrapIfNeeded() {
        if (currentTileee.getDirection().equalsIgnoreCase("trap")) {
            player = new LinearLayout(Dungeon.this);
            player.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
            playerImage = new ImageView(Dungeon.this);
            playerImage.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
            Glide.with(Dungeon.this).load(R.drawable.charactertrap).into(playerImage);
            currentTile.removeAllViews();
            player.addView(playerImage);
            currentTile.addView(player);
            currentTileee.setDirection("floor");
        }
    }

    private void handleItemObtaining() {
        String itemObtained = "";
        if (currentTile.getChildCount() > 0) {

            switch (currentTileee.getDirection()) {

                case "key1":
                    itemObtained = "Llave 1";
                    break;

                case "key2":
                    itemObtained = "Llave 2";
                    break;

                case "key3":
                    itemObtained = "Llave 3";
                    break;

                case "trap":
                    itemObtained = "Trampa";
                    break;

            }

            if (!itemObtained.equalsIgnoreCase("Trampa")) {
                message_box.setText("Has obtenido " + itemObtained);
                currentTile.removeAllViews();
                SharedPreferences.Editor editor = preferences.edit();
                editor.putBoolean(itemObtained, true);
                editor.apply();
            }
        }
    }

    private void spawnPlayer(LinearLayout cell, String direction) {
        player = new LinearLayout(Dungeon.this);
        player.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        playerImage = new ImageView(Dungeon.this);
        playerImage.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        player.addView(playerImage);
        switch (direction) {

            case "up":
                Glide.with(Dungeon.this).load(R.drawable.backcharacter).into(playerImage);
                break;
            case "down":
                Glide.with(Dungeon.this).load(R.drawable.frontcharacter).into(playerImage);
                break;

            case "left":
                Glide.with(Dungeon.this).load(R.drawable.leftcharacter).into(playerImage);
                break;

            case "right":
                Glide.with(Dungeon.this).load(R.drawable.rightcharacter).into(playerImage);
                break;

        }
        cell.addView(player);
        currentTile = cell;
    }

    private void implementListeners() {
        for (int i = 0; i < cells.length; i++) {
            for (int j = 0; j < cells[i].length; j++) {
                int finalI = i;
                int finalJ = j;
                cells[i][j].setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(Dungeon.this, "Este tile es: " + tiles[finalI][finalJ].getDirection(), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }
    }

    private void giveGenericWallsItsDirection() {
        for (int i = 0; i < cells.length; i++) {
            for (int j = 0; j < cells[i].length; j++) {
                if (!tiles[i][j].isFloor()) {
                    tiles[i][j].setDirection("wall");
                }
            }
        }
    }

    private void buildCorners() {
        for (int i = 0; i < cells.length; i++) {
            for (int j = 0; j < cells[i].length; j++) {

                try {

                    if (!tiles[i][j].isFloor() && tiles[i + 1][j].getDirection().equals("west") && tiles[i][j + 1].getDirection().equals("north")) {
                        cells[i][j].setBackground(getResources().getDrawable(R.drawable.topleftcorner)); //Arriba izq
                        tiles[i][j].setDirection("topleft");
                    } else if (!tiles[i][j].isFloor() && tiles[i + 1][j].getDirection().equals("east") && tiles[i][j - 1].getDirection().equals("north")) {
                        cells[i][j].setBackground(getResources().getDrawable(R.drawable.toprightcorner)); //Arriba derecha
                        tiles[i][j].setDirection("topright");
                    } else if (!tiles[i][j].isFloor() && tiles[i][j].getDirection().equals("west") && tiles[i + 1][j + 1].getDirection().equals("south")) {
                        cells[i + 1][j].setBackground(getResources().getDrawable(R.drawable.bottomleftcorner)); //Abajo izq
                        tiles[i + 1][j].setDirection("bottomleft");
                    } else if (!tiles[i][j].isFloor() && tiles[i][j].getDirection().equals("east") && tiles[i + 1][j - 1].getDirection().equals("south")) {
                        cells[i + 1][j].setBackground(getResources().getDrawable(R.drawable.bottomrightcorner)); //Abajo derecha
                        tiles[i + 1][j].setDirection("bottomright");
                    }

                }catch (Exception e){
                }

            }
        }
    }

    private void setFloorAndWalls() {
        for (int i = 0; i < tiles.length; i++) {
            for (int j = 0; j < tiles[i].length; j++) {
                cells[i][j].setBackground(getResources().getDrawable(R.drawable.wall));
                if (tiles[i][j].isFloor()) {
                    cells[i][j].setBackground(getResources().getDrawable(R.drawable.floor));
                    tiles[i][j].setDirection("floor");
                } else {
                    if ((i - startingI < floorRows) && (j - startingJ < floorCols)) {
                        if (i >= startingI && j >= startingJ) {
                            cells[i][j].setBackground(getResources().getDrawable(R.drawable.floor));
                            tiles[i][j].setFloor(true);
                            tiles[i][j].setDirection("floor");
                        }
                    }
                }
            }
        }
    }

    private void buildWalls() {
        for (int i = 0; i < tiles.length; i++) {
            for (int j = 0; j < tiles[i].length; j++) {
                if (tiles[i][j].isFloor() == false) {

                    try {
                        if (i < tiles.length-1) {
                            if (tiles[i + 1][j].isFloor()) {
                                cells[i][j].setBackground(getResources().getDrawable(R.drawable.northwall));
                                tiles[i][j].setDirection("north");
                            } else if (j < tiles[i].length - 1) {
                                if (tiles[i][j + 1].isFloor()) {
                                    cells[i][j].setBackground(getResources().getDrawable(R.drawable.westwall));
                                    tiles[i][j].setDirection("west");
                                }
                            }
                        }
                        if (j > 0) {
                            if (tiles[i][j - 1].isFloor()) {
                                cells[i][j].setBackground(getResources().getDrawable(R.drawable.eastwall));
                                tiles[i][j].setDirection("east");
                            }
                        }
                    } catch (ArrayIndexOutOfBoundsException e){
                        //cells[i][j].setBackground(getResources().getDrawable(R.drawable.ic_launcher_background));
                        //tiles[i][j].setDirection("east");
                    }

                } else {

                    try {
                        if (!tiles[i + 1][j].isFloor()) {
                            cells[i + 1][j].setBackground(getResources().getDrawable(R.drawable.southwall));
                            tiles[i + 1][j].setDirection("south");
                        } else if (!tiles[i][j - 1].isFloor()) {
                            cells[i][j - 1].setBackground(getResources().getDrawable(R.drawable.westwall));
                            tiles[i][j - 1].setDirection("west");
                        } else if (!tiles[i][j + 1].isFloor()) {
                            cells[i][j + 1].setBackground(getResources().getDrawable(R.drawable.ic_launcher_background));
                            tiles[i][j + 1].setDirection("east");
                        }
                    } catch (ArrayIndexOutOfBoundsException e){
                    }

                }
            }
        }
    }
}