package com.dungeons.dungeon1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.dungeons.dungeon1.dungeonscode.Dungeon;

public class MainActivity extends AppCompatActivity {

    private Button dungeonBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        assignComponents();
        implementListeners();

    }

    private void implementListeners() {

        dungeonBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, Dungeon.class);
                startActivity(i);
            }
        });

    }

    private void assignComponents() {
        dungeonBtn = findViewById(R.id.toDungeonBtn);
    }
}