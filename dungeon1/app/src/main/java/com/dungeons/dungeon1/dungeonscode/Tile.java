package com.dungeons.dungeon1.dungeonscode;

public class Tile {

    private boolean isFloor;
    private String direction;

    public Tile() {
    }

    public Tile(boolean isFloor) {
        this.isFloor = isFloor;
    }

    public Tile(boolean isFloor, String direction) {
        this.isFloor = isFloor;
        this.direction = direction;
    }

    public boolean isFloor() {
        return isFloor;
    }

    public void setFloor(boolean floor) {
        isFloor = floor;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }
}
